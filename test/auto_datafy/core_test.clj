(ns auto-datafy.core-test
  (:require [auto-datafy.core :as sut]
            [clojure.datafy :as d]
            [clojure.test :refer :all]))

;; TODO TEST static getters are excluded
;; TODO kebab-case ?
;; TODO strip get from getters?

(definterface HasGetter (getValue []))

(deftype Foo1 [foo])
(sut/extend-datafy Foo1)
(deftest fields-are-included-test
  (is (= {:-foo "foo"} (d/datafy (Foo1. "foo")))))

(deftype Foo2 []
  HasGetter
  (getValue [this] 42))
(sut/extend-datafy Foo2)
(deftest getters-are-included-test
  (is (= {:getValue 42} (d/datafy (Foo2.)))))

(definterface DoesThing (doThing [x y]))
(deftype Foo3 []
  HasGetter
  (getValue [this] 42)
  DoesThing
  (doThing [this x y] (throw (Exception. "dont call me!"))))
(sut/extend-datafy Foo3)
(deftest other-methods-are-excluded-test
  (is (= {:getValue 42} (d/datafy (Foo3.)))))

(deftype Foo4 [Foo1]
  HasGetter
  (getValue [this] (Foo1. "getter-foo")))
(sut/extend-datafy Foo4)
(deftest recursively-datafies-test
  (is (= {:getValue {:-foo "getter-foo"} :-Foo1 {:-foo "field-foo"}}
         (d/datafy (Foo4. (Foo1. "field-foo"))))))
