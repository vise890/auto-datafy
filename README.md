# auto-datafy [![pipeline status](https://gitlab.com/vise890/auto-datafy/badges/master/pipeline.svg)](https://gitlab.com/vise890/auto-datafy/commits/master)

Naively implements `clojure.core.protocols.Datafy` for trivial classes.

## [API Docs](https://vise890.gitlab.io/auto-datafy/)

## Usage

[![Clojars Project](https://img.shields.io/clojars/v/vise890/auto-datafy.svg)](https://clojars.org/vise890/auto-datafy)

```clojure
;;; project.clj
[vise890/auto-datafy "0.1.0"]
```

```clojure
(deftype Foo [foo])

(require 'auto-datafy.core)
(extend-datafy Foo)

(require 'clojure.datafy)
(datafy (Foo1. "foo"))
;; => {:-foo "foo"}
```

## License

Copyright © 2019 Martino Visintin

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
