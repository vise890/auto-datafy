(ns auto-datafy.core
  (:require [clojure.core.protocols :as p]
            [clojure.datafy :as d]
            [clojure.reflect :as r]
            [clojure.string :as str]))

(defn- flagged-with?
  [member flag]
  (contains? (:flags member) flag))

(defn- field? [member] (instance? clojure.reflect.Field member))
(defn- method? [member] (instance? clojure.reflect.Method member))

(defn- getter?
  [{:keys [flags parameter-types] :as member}]
  (and (method? member)
       (str/starts-with? (str (:name member)) "get")
       (empty? parameter-types)))

(defprotocol KeywordNamed (kw-name [this]))
(extend-protocol KeywordNamed
  clojure.reflect.Field
  (kw-name [this]
    (keyword (str "-" (:name this))))
  clojure.reflect.Method
  (kw-name [this]
    (keyword (:name this))))

(defmacro extend-datafy

  "Naively implements `clojure.core.protocols.Datafy` for `klass`. Includes all
  `public`, non-`static` methods starting with \"`get`\" and having no
  parameters and all `public`, non-`static` fields."

  [klass]
  (let [r                (r/reflect (resolve klass))
        include?         (fn [member]
                           (and (flagged-with? member :public)
                                (not (flagged-with? member :static))
                                (or (getter? member)
                                    (field? member))))
        included-members (filter include? (:members r))
        this-sym         (gensym "this")]
    `(extend-type ~klass
       p/Datafiable
       (p/datafy [~this-sym]
         ~(->> included-members
               (map (fn [mmb] [(kw-name mmb)
                              `(d/datafy (. ~this-sym ~(:name mmb)))]))
               (into {}))))))


